set viminfo="NONE"
set nobackup
set nowb
set noswapfile
"set backupdir=~/.cache/vim/backups
"set directory=~/.cache/vim/swaps
"set backupskip=/tmp/*,/private/tmp/*

let g:netrw_dirhistmax = 0

set esckeys
set backspace=indent,eol,start
set ttyfast
set encoding=utf-8 nobomb
set number
set cursorline
set tabstop=4 softtabstop=4 expandtab shiftwidth=4 smarttab
set lcs=tab:▸\ ,trail:·,eol:¬,nbsp:_
set ffs=unix,dos,mac
set showmatch
set hlsearch
set incsearch
set nostartofline
set ruler
set showmode
set title

syntax on
